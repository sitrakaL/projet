package com.projet.java.gestion_stock.services;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.projet.java.gestion_stock.repositories.ProduitRepository;

@Service
public class ProduitServiceImpl {
	@Autowired
	ProduitRepository produit;
	
	int seqValPro() {
		return produit.seqValPro();
	}
}
