package com.projet.java.gestion_stock.services;

import java.util.List;

import com.projet.java.gestion_stock.models.BondeEntree;

public interface BondeEntreeService {

	List<BondeEntree> getBondeEntreeByIdProd(String idProd);
}
