package com.projet.java.gestion_stock.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projet.java.gestion_stock.models.Produit;
import org.springframework.data.jpa.repository.Query;


public interface ProduitRepository extends JpaRepository<Produit, String>{
	

	@Query(value = "select nextval('numproduit_seq');", nativeQuery = true)
	int seqValPro();
	
	@Query(value = "select * from produit where design like CONCAT( ?,'%') OR num_produit like CONCAT( ?,'%') ;", nativeQuery = true)
	List<Produit> rechercheProd(String design, String num);
	
	@Query(value = "select design,stock from produit ;", nativeQuery = true)
	List<String> etatDeStock();
	
	@Query(value = "select bondeentree.numbonentree,\r\n" + 
			"bondeentree.qteentree, bondeentree.dateentree from produit inner join bondeentree \r\n" + 
			"on produit.num_produit = bondeentree.numproduit\r\n" + 
			"where produit.design = ?;", nativeQuery = true)
	List<String> etatMouvementStockEntree(String design);
	
	@Query(value = "select bondesortie.numbonsortie, bondesortie.datesortie, \r\n" + 
			"bondesortie.qtesortie from produit\r\n" + 
			"inner join bondesortie on produit.num_produit = bondesortie.numproduit where produit.design = ?;", nativeQuery = true)
	List<String> etatMouvementStockSortie(String design);
}
