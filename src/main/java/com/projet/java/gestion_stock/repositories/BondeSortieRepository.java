package com.projet.java.gestion_stock.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.projet.java.gestion_stock.models.BondeSortie;

public interface BondeSortieRepository extends JpaRepository<BondeSortie, String> {
	@Query(value = "select nextval('numbonsortie_seq');", nativeQuery = true)
	int seqValBondSortie();
	
	@Query(value="Select*from bondesortie where numproduit = ?", nativeQuery = true)
	List<BondeSortie> getBondeSortieByIdProd(String idProd);
}
