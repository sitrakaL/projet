package com.projet.java.gestion_stock.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.projet.java.gestion_stock.models.BondeEntree;

public interface BondeEntreeRepository extends JpaRepository<BondeEntree, String> {

	@Query(value = "select nextval('numbonentree_seq');", nativeQuery = true)
	int seqValBondEntree();
	
	@Query(value="Select*from bondeentree where numproduit = ?", nativeQuery = true)
	List<BondeEntree> getBondeEntreeByIdProd(String idProd);


}
