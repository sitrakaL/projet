package com.projet.java.gestion_stock.Config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.projet.java.gestion_stock.controller.BondeEntreeController;
import com.projet.java.gestion_stock.controller.HelloController;
import com.projet.java.gestion_stock.controller.ProduitController;
import com.projet.java.gestion_stock.controller.BondeSortieController;


@Component
@ApplicationPath("api/")
public class JerseyConfig extends ResourceConfig{
	public JerseyConfig() {
//		register(HelloController.class);
		register(ProduitController.class);
		register(BondeEntreeController.class);
		register(BondeSortieController.class);

		
	}
}
