package com.projet.java.gestion_stock.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.projet.java.gestion_stock.models.AutresFonctions;
import com.projet.java.gestion_stock.models.Produit;
import com.projet.java.gestion_stock.repositories.ProduitRepository;
import com.projet.java.gestion_stock.services.ProduitServiceImpl;


@Component
@Path("/produit")
public class ProduitController {
	
//	private static final Logger logger = LoggerFactory.getLogger(GestionStockApplication.class);
	
	@Autowired
	ProduitServiceImpl service;
	
	@Autowired
	ProduitRepository ripo;
	
	AutresFonctions func = new AutresFonctions();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveProduit(Produit p) {
		try {
			Produit prod = new Produit();
			prod.setNum_produit(func.genererIdProduit(ripo.seqValPro()));
			prod.setDesign(p.getDesign());
			prod.setStock(p.getStock());
			ripo.save(prod);		
			return Response.status(200).entity(prod).build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response GetAllProduit() {
		try {
			List<Produit> list = new ArrayList<Produit>();
			list  = ripo.findAll();
			return Response.status(200).entity(list).build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response DeleteProd(@PathParam("id") String id) {
		try{
			Optional<Produit> prod = ripo.findById(id);
				ripo.delete(prod.get());
				return Response.status(200).entity("Delete successfully").build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}

	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response UpdateProd(Produit p) {
		try {
			Optional<Produit> prod = ripo.findById(p.getNum_produit());
			Produit mod = prod.get();
			mod.setDesign(p.getDesign());
			mod.setStock(p.getStock());
			ripo.save(mod);
			return Response.status(200).entity(mod).build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response GetProdById(@PathParam("id") String id) {
		try {
			Optional<Produit> prod = ripo.findById(id);
			Produit mod = prod.get();
			return Response.status(200).entity(mod).build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}
	
	@GET
	@Path("recherche/{design}/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rechercheProdByDesignCode(@PathParam("design") String design,
			@PathParam("code") String code) {
		try {
			List<Produit> p = ripo.rechercheProd(design, code);
			return Response.status(200).entity(p).build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}
	
	@GET
	@Path("etat")
	@Produces(MediaType.APPLICATION_JSON)
	public Response EtatStock() {
		try {
			List<String> chaine = ripo.etatDeStock();
			return Response.status(200).entity(chaine).build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("etatMouvementP/{design}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response etatMouvementP(@PathParam("design") String design) {
		try {
			List<String> entree = ripo.etatMouvementStockEntree(design);
			List<String> sortie = ripo.etatMouvementStockSortie(design);
			List<List<String>> res = new ArrayList<>();
			res.add(entree);
			res.add(sortie);
			return Response.status(200).entity(res).build();
			
		}catch (Exception e) {
			e.printStackTrace();
			return Response.ok(e, MediaType.APPLICATION_JSON).build();
		}
	}

}
