package com.projet.java.gestion_stock.controller;


import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.java.gestion_stock.GestionStockApplication;
import com.projet.java.gestion_stock.models.AutresFonctions;
import com.projet.java.gestion_stock.models.BondeEntree;
import com.projet.java.gestion_stock.models.Produit;
import com.projet.java.gestion_stock.repositories.BondeEntreeRepository;
import com.projet.java.gestion_stock.repositories.ProduitRepository;


@Component
@Path("/bondeEntree")
public class BondeEntreeController {
	
	 private static final Logger logger = LoggerFactory.getLogger(GestionStockApplication.class);
	 private static final ObjectMapper om = new ObjectMapper();

	 
	 AutresFonctions func = new AutresFonctions();

	 @Autowired
	 BondeEntreeRepository ripo;
	 
	 @Autowired
	 ProduitRepository pr;
		
		
	 @POST
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response saveBondeEntree(BondeEntree bd) {
		try {
				logger.info("#Bd:" + om.writeValueAsString(bd));
				BondeEntree entr = new BondeEntree();
				Produit produit = pr.findById(bd.getNumProduit().getNum_produit()).orElse(new Produit());
					entr.setNumBonEntree(func.genererIdBondeEntree(ripo.seqValBondEntree()));
					entr.setNumProduit(produit);
					entr.setQteEntree(bd.getQteEntree());
					entr.setDateEntree(func.dateAndroany());
					ripo.save(entr);				
					produit.setStock(produit.getStock() + bd.getQteEntree());
					pr.save(produit);				
					return Response.status(200).entity(entr).build();
				
		}catch(Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}			
		}
	 
	    @DELETE
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response DeleteProd(@PathParam("id") String id) {
			try{
					BondeEntree bond = ripo.findById(id).orElse(new BondeEntree ());
					Produit produit = pr.findById(bond.getNumProduit().getNum_produit()).orElse(new Produit());
					produit.setStock(produit.getStock() - bond.getQteEntree());
					pr.save(produit);
					ripo.delete(bond);
					return Response.status(200).entity("Cette transaction a été éffacée avec succès").build();
				
			}catch (Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}

		}
	    
	    @GET
		@Produces(MediaType.APPLICATION_JSON)
		public Response GetAllProduit() {
			try {
				List<BondeEntree> list = new ArrayList<BondeEntree>();
				list  = ripo.findAll();
				return Response.status(200).entity(list).build();
			}catch (Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}
		}
	    
		@GET
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response GetProdById(@PathParam("id") String id) {
			try {
				BondeEntree bond = ripo.findById(id).orElse(new BondeEntree ());
				return Response.status(200).entity(bond).build();
				
			}catch (Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}
		}

		@GET
		@Path("bondeEntreeByProd/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response GetProdByIdProd(@PathParam("id") String id) {
			try {
				List<BondeEntree> bond = ripo.getBondeEntreeByIdProd(id);
				return Response.status(200).entity(bond).build();
				
			}catch (Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}
		}
		
		@PUT
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response UpdateProd(BondeEntree bond) {
			try {
				BondeEntree b = ripo.findById(bond.getNumBonEntree()).orElse(new BondeEntree ());
				Produit produit = pr.findById(bond.getNumProduit().getNum_produit()).orElse(new Produit());
				int ancienStock = produit.getStock() - b.getQteEntree();
				int newStock = ancienStock + bond.getQteEntree();
				b.setDateModification(func.dateAndroany());
				b.setQteEntree(bond.getQteEntree());
				produit.setStock(newStock);
				b.setNumProduit(produit);
				pr.save(produit);
				ripo.save(b);
				return Response.status(200).entity(b).build();
			}catch (Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}
		}
		
}
