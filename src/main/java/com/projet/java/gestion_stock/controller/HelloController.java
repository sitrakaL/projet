package com.projet.java.gestion_stock.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.projet.java.gestion_stock.models.Produit;
import com.projet.java.gestion_stock.repositories.ProduitRepository;

@Component
@Path("/hello")
public class HelloController {
	@Autowired
	private ProduitRepository rep;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Produit> hello() {
		return rep.findAll();
	}
	
}
