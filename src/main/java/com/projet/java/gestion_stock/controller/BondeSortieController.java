package com.projet.java.gestion_stock.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projet.java.gestion_stock.GestionStockApplication;
import com.projet.java.gestion_stock.models.AutresFonctions;
import com.projet.java.gestion_stock.models.BondeEntree;
import com.projet.java.gestion_stock.models.BondeSortie;
import com.projet.java.gestion_stock.models.Produit;
import com.projet.java.gestion_stock.repositories.BondeEntreeRepository;
import com.projet.java.gestion_stock.repositories.BondeSortieRepository;
import com.projet.java.gestion_stock.repositories.ProduitRepository;

@Component
@Path("/bondeSortie")
public class BondeSortieController {

	 private static final Logger logger = LoggerFactory.getLogger(GestionStockApplication.class);
	 private static final ObjectMapper om = new ObjectMapper();
	 
	 AutresFonctions func = new AutresFonctions();

	 @Autowired
	 BondeEntreeRepository entre;
	 
	 @Autowired
	 BondeSortieRepository sort;
	 
	 @Autowired
	 ProduitRepository pr;
	 
	 @POST
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response saveBondeSortie(BondeSortie bd) {
		try {
				logger.info("#Bd:" + om.writeValueAsString(bd));
				BondeSortie sortie = new BondeSortie();
				Produit produit = pr.findById(bd.getNumProduit().getNum_produit()).orElse(new Produit());
				if(func.etatStock(produit.getStock(), bd.getQteSortie()) > 0) {
					sortie.setNumBonSotie(func.genererIdBondeSortie(sort.seqValBondSortie()));
					sortie.setNumProduit(produit);
					sortie.setQteSortie(bd.getQteSortie());
					sortie.setDateSortie(func.dateAndroany());
					sort.save(sortie);
					produit.setStock(produit.getStock() - bd.getQteSortie());
					pr.save(produit);				
					return Response.status(200).entity(sortie).build();
				}
				else {
					return Response.status(200).entity("La quantité de la stock ne permet pas cette transaction").build();
					
				}
					
		}catch(Exception e) {
				e.printStackTrace();
				return Response.ok(e, MediaType.APPLICATION_JSON).build();
			}			
		}
	 
	  		@DELETE
			@Path("/{id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response DeleteProd(@PathParam("id") String id) {
				try{
					    BondeSortie bond = sort.findById(id).orElse(new BondeSortie ());
						Produit produit = pr.findById(bond.getNumProduit().getNum_produit()).orElse(new Produit());
						produit.setStock(produit.getStock() + bond.getQteSortie());
						pr.save(produit);
						sort.delete(bond);
						return Response.status(200).entity("Cette transaction a été éffacée avec succès").build();
					
				}catch (Exception e) {
					e.printStackTrace();
					return Response.ok(e, MediaType.APPLICATION_JSON).build();
				}

			}
	  		
	  		 @GET
	 		@Produces(MediaType.APPLICATION_JSON)
	 		public Response GetAllProduit() {
	 			try {
	 				List<BondeSortie> list = new ArrayList<BondeSortie>();
	 				list  = sort.findAll();
	 				return Response.status(200).entity(list).build();
	 			}catch (Exception e) {
	 				e.printStackTrace();
	 				return Response.ok(e, MediaType.APPLICATION_JSON).build();
	 			}
	 		}
	  		 
	  		@GET
			@Path("/{id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response GetProdById(@PathParam("id") String id) {
				try {
					BondeSortie bond = sort.findById(id).orElse(new BondeSortie ());
					return Response.status(200).entity(bond).build();
					
				}catch (Exception e) {
					e.printStackTrace();
					return Response.ok(e, MediaType.APPLICATION_JSON).build();
				}
			}
	  		
	  		@GET
			@Path("bondeSortieByProd/{id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response GetProdByIdProd(@PathParam("id") String id) {
				try {
					List<BondeSortie> bond = sort.getBondeSortieByIdProd(id);
					return Response.status(200).entity(bond).build();
					
				}catch (Exception e) {
					e.printStackTrace();
					return Response.ok(e, MediaType.APPLICATION_JSON).build();
				}
			}
	  		
	  		@PUT
			@Consumes(MediaType.APPLICATION_JSON)
			@Produces(MediaType.APPLICATION_JSON)
			public Response UpdateProd(BondeSortie bond) {
				try {
					BondeSortie b = sort.findById(bond.getNumBonSortie()).orElse(new BondeSortie ());
					Produit produit = pr.findById(bond.getNumProduit().getNum_produit()).orElse(new Produit());
					int ancienStock = produit.getStock() + b.getQteSortie();
					int newStock = ancienStock - bond.getQteSortie();
					if(func.etatStock(ancienStock, bond.getQteSortie()) > 0) {
						b.setDateModification(func.dateAndroany());
						b.setQteSortie(bond.getQteSortie());
						produit.setStock(newStock);
						b.setNumProduit(produit);
						pr.save(produit);
						sort.save(b);
						return Response.status(200).entity(b).build();
					}else {
						return Response.status(200).entity("La quantité de la stock ne permet pas cette transaction").build();
					}
					
				}catch (Exception e) {
					e.printStackTrace();
					return Response.ok(e, MediaType.APPLICATION_JSON).build();
				}
			}
			
	 
}
