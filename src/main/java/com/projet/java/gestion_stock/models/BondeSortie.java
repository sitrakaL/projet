package com.projet.java.gestion_stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "BondeSortie")
public class BondeSortie {

	@Id
	@Column(name = "NumBonSortie", unique = true, nullable = false)
	private String NumBonSortie;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name ="NumProduit")
	private Produit NumProduit;
	
	@Column(name = "qteSortie", nullable = false)
	private int qteSortie;
	
	@Column(name = "dateSortie", nullable = true)
	private String dateSortie;
	
	@Column(name = "dateModification", nullable = true)
	private String dateModification;

	
	public BondeSortie(String numBonSotie, Produit numProduit, int qteSortie, String dateSortie,
			String dateModification) {
		super();
		NumBonSortie = numBonSotie;
		NumProduit = numProduit;
		this.qteSortie = qteSortie;
		this.dateSortie = dateSortie;
		this.dateModification = dateModification;
	}

	public BondeSortie() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNumBonSortie() {
		return NumBonSortie;
	}

	public void setNumBonSotie(String numBonSotie) {
		NumBonSortie = numBonSotie;
	}

	public Produit getNumProduit() {
		return NumProduit;
	}

	public void setNumProduit(Produit numProduit) {
		NumProduit = numProduit;
	}

	public int getQteSortie() {
		return qteSortie;
	}

	public void setQteSortie(int qteSortie) {
		this.qteSortie = qteSortie;
	}

	public String getDateSortie() {
		return dateSortie;
	}

	public void setDateSortie(String dateSortie) {
		this.dateSortie = dateSortie;
	}

	public String getDateModification() {
		return dateModification;
	}

	public void setDateModification(String dateModification) {
		this.dateModification = dateModification;
	}

}
