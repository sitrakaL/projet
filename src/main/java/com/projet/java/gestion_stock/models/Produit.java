package com.projet.java.gestion_stock.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Produit")
public class Produit {
	@Id
	@Column(name = "num_produit", unique = true, nullable = false)
	private String num_produit;
	
	@Column(name = "design", unique = true, nullable = false)
	private String design;
	
	@Column(name = "stock", nullable = false)
	private int stock;

	public String getNum_produit() {
		return num_produit;
	}

	public void setNum_produit(String num_produit) {
		this.num_produit = num_produit;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Produit(String num_produit, String design, int stock) {
		super();
		this.num_produit = num_produit;
		this.design = design;
		this.stock = stock;
	}

	public Produit() {
		super();
	}
	
	
}
