package com.projet.java.gestion_stock.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class AutresFonctions {

	public String genererIdProduit(long sequence) {
		Date daty = new Date();
		String datyy = convertDateToString6(daty);
		return "PROD"+datyy+String.format("%04d",sequence );
		
		}
	
	public String genererIdBondeEntree(long sequence) {
		return "BE"+String.format("%02d",sequence );
		
		}
	
	public String genererIdBondeSortie(long sequence) {
		return "BS"+String.format("%02d",sequence );
		
		}
	
	public  static String convertDateToString6(Date daty) {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		return df.format(daty);
	}
	
	public String dateAndroany() {
		String pattern = "dd-MM-YYY";
		Date daty = new Date();
		DateFormat df = new SimpleDateFormat(pattern);	
		return df.format(daty);
	}
	
	public int calculStock(int AncienqteEntre, int nouveauQteEntre) {
		if(AncienqteEntre < nouveauQteEntre) {
			return nouveauQteEntre - AncienqteEntre;
		}
		return AncienqteEntre - nouveauQteEntre;
		
	}
	
	public int etatStock(int stock, int qteSortie) {
		
		if(stock >= qteSortie) {
			return stock - qteSortie;
		}
		else {
			return -1;
		}
	}
}
