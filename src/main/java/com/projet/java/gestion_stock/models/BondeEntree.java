package com.projet.java.gestion_stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Table(name = "BondeEntree")
public class BondeEntree {
	@Id
	@Column(name = "NumBonEntree", unique = true, nullable = false)
	private String NumBonEntree;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name ="NumProduit")
	private Produit NumProduit;
	
	@Column(name = "qteEntree", nullable = false)
	private int qteEntree;
	
	@Column(name = "dateEntree", nullable = true)
	private String dateEntree;

	@Column(name = "dateModification", nullable = true)
	private String dateModification;
	
	public String getDateModification() {
		return dateModification;
	}

	public void setDateModification(String dateModification) {
		this.dateModification = dateModification;
	}

	public String getNumBonEntree() {
		return NumBonEntree;
	}

	public void setNumBonEntree(String numBonEntree) {
		NumBonEntree = numBonEntree;
	}

	public Produit getNumProduit() {
		return NumProduit;
	}

	public void setNumProduit(Produit produit) {
		NumProduit = produit;
	}

	public int getQteEntree() {
		return qteEntree;
	}

	public void setQteEntree(int qteEntree) {
		this.qteEntree = qteEntree;
	}

	public String getDateEntree() {
		return dateEntree;
	}

	public void setDateEntree(String dateEntree) {
		this.dateEntree = dateEntree;
	}

	public BondeEntree() {
		super();
	}

	public BondeEntree(String numBonEntree, Produit numProduit, int qteEntree, String dateEntree) {
		super();
		NumBonEntree = numBonEntree;
		NumProduit = numProduit;
		this.qteEntree = qteEntree;
		this.dateEntree = dateEntree;
	}
	
	
}
